# magnet-scrape
Are you tired of writing fragile web scraping libraries on a per site basis?
Why not use a library that works for every site that has magnet links in their responses?
The response can be in `html`, `json`, `xml`, or any other format that contains magnets.

* ***Modeled after the Energizer Bunny***
* ***Makes no unnecessary assumptions***
* ***Doesn't break when sites update***
* ***It's a complete solution when paired with tracker scraping to get peer counts***
* ***Only has one optional dependency:*** `entities`

The CLI tool can be found [here.](https://github.com/pro-src/magnet-scrape-cli)

```js
const scrape = require('magnet-scrape');
const request = require('request');

const uri = 'https://thepiratebay.org/search/ubuntu%20desktop/0/99/300';

request(uri, (error, response) => {
  if (error) throw error;
  
  const torrents = scrape(response.body);
  console.log(torrents);
});
```

##### Returns:
```js
[
  {
    "announce": [
      "udp://tracker.leechers-paradise.org:6969",
      "udp://zer0day.ch:1337",
      "udp://open.demonii.com:1337",
      "udp://tracker.coppersurfer.tk:6969",
      "udp://exodus.desync.com:6969"
    ],
    "infoHash": "E84213A794F3CCD890382A54A64CA68B7E925433",
    "name": "Ubuntu 18.04.1 Desktop 64 bit",
    "magnet": "magnet:?xt=urn:btih:e84213a794f3ccd890382a54a64ca68b7e925433&dn=Ubuntu+18.04.1+Desktop+64+bit&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969"
  },
  ...
]
```

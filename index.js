const patterns = (function() {
  const chars = '(?:%[a-f0-9]{2}|[a-z0-9-_.!~*\'()+])+';
  const minusApos = chars.replace(/'/g, '');

  return {
    xt: new RegExp('^urn:btih:([a-f0-9]{40})', 'i'),
    xl: new RegExp('^\d+'),
    dn: new RegExp('^' + chars, 'i'),
    tr: new RegExp('^(?:https?|udp|wss?)%3A%2F%2F' + minusApos, 'i')
  };
}());

const decodeEntities = (function() {
  try {
    return require('entities/lib/decode').HTML;
  }
  catch(error) {
    if (error.code !== 'MODULE_NOT_FOUND') {
      throw error;
    }

    return function(str){
      return str;
    };
  }
}());

module.exports = scrape;

function scrape(content) {
  if (typeof content !== 'string') {
    throw new TypeError('Expected content to be type of string, '
      + `got type of ${typeof content} instead.`);
  }

  const results = [];
  const hashes = [];

  const re = /(?:^\?|&(?=amp;)?)([a-z]{2})=([^&]+)/gi;
  content = content.split(/magnet:/i).slice(1);

  let chunk;
  while ((chunk = content.shift()) !== undefined) {
    const torrent = { announce: [] };
    const params = [];

    let match;
    loop: while ((match = re.exec(chunk)) !== null) {
      const name  = match[1].toLowerCase();
      const value = match[2];

      switch (name) {
        case 'xt': // eXact Topic
          if ((match = value.match(patterns.xt)) !== null) {
            torrent.infoHash = match[1].toUpperCase();

            params.unshift(name + '=' + match[0]);
            continue loop;
          }

          break loop;
        case 'xs': // eXact Source
        case 'as': // Acceptable Source
        case 'ws': // Web Source
        case 'tr': // Tracker
          if ((match = value.match(patterns.tr)) !== null) {
            if (name === 'tr') {
              torrent.announce.push(decodeURIComponent(match[0]));
            }

            params.push(name + '=' + match[0]);
            continue loop;
          }

          break loop;
        case 'kt': // Keywords
        case 'dn': // Display Name
          if ((match = value.match(patterns.dn)) !== null) {
            const decodedValue = decodeEntities(match[0]);

            if (name === 'dn') {
              torrent.name = decodeURIComponent(decodedValue)
                .replace(/\+/g, ' ');
            }

            params.push(name + '=' + decodedValue);
            continue loop;
          }

          break loop;
        case 'ix': // File Index
        case 'xl': // eXact Length
          if ((match = value.match(patterns.xl)) !== null) {
            if (name === 'xl') {
              torrent.length = parseInt(match[0]);
            }

            params.push(name + '=' + match[0]);
            continue loop;
          }
        default:
          break loop;
      }
    }

    if (torrent.infoHash !== undefined &&
        -1 === hashes.indexOf(torrent.infoHash)) {
      hashes.push(torrent.infoHash);

      torrent.magnet = 'magnet:?' + params.join('&');
      results.push(torrent);
    }
  }

  return results;
};
